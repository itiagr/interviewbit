int Solution::removeElement(vector<int> &A, int B) {
    int n = A.size();
    int newLength = 0;

    for(int i=0 ; i<n ; i++) {
        if(A[i] != B) {
            A[newLength] = A[i];
            newLength++;
        }
    }

    return newLength;
}
