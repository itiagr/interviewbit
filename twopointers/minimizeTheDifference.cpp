int minimun(int num1, int num2, int num3) {
	return min( num1 , min( num2 , num3));
}

int maximum(int num1, int num2, int num3) {
	return max( num1 , max( num2 , num3));
}

int Solution::solve(vector<int> &A, vector<int> &B, vector<int> &C) {
	int answer = INT_MAX;
	int indexA = 0, indexB = 0, indexC = 0;
	int numA = A.size(), numB = B.size(), numC = C.size();

	while(indexA < numA && indexB < numB && indexC < numC) {
		int maxVal = maximum(A[indexA], B[indexB], C[indexC]);
		int minVal = minimun(A[indexA], B[indexB], C[indexC]);
		int diff = abs( maxVal -  minVal ); 
		answer = min(answer, diff);

		if(A[indexA] == minVal)	indexA++;
		else if(B[indexB] == minVal) indexB++;
		else indexC++;
	}

	return answer;
}