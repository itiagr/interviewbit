int Solution::threeSumClosest(vector<int> &A, int B) {
	int n = A.size();
	int answer = -1, minDiff = INT_MAX;
	std::sort (A.begin(), A.end()); 

	for(int i=0 ; i<n-2 ; i++){
		int left = i+1, right = n-1;

		while(left < right) {
			int curSum = (A[i] + A[left] + A[right]);

			if(curSum == B){
				return B;
			}

			int curDiff = abs(B - curSum);
			
			if( curDiff < minDiff) {
				minDiff = curDiff;
				answer = curSum;
			}

			if(curSum < B)	left++;
			else right--;
		}
	}   
	return answer;
}