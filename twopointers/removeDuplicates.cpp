int Solution::removeDuplicates(vector<int> &A) {
    int n = A.size();

    if(n==0 || n==1) return n;
    
    int firstP = 1, secondP = 1;

    while(firstP < n){
        if(A[firstP] != A[firstP-1]) {
            A[secondP] = A[firstP];
            secondP++;
        }
        firstP++;
    }

    while(n != secondP) {
        A.pop_back();
        n--;
    }

    return secondP;
}
