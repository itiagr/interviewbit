vector<vector<int> > Solution::threeSum(vector<int> &A) {
	vector<vector<int> > answer;

	std::sort( A.begin(), A.end() );
	int n = A.size();

	for(int i=0 ; i<n-2 ; i++){
		int left = i+1, right = n-1;

		while(left < right) {
			int curSum = (A[i] + A[left] + A[right]);

			if(curSum == 0 ){
				vector<int> v{ A[i], A[left], A[right] };
				if(find(answer.begin(), answer.end(), v) == answer.end()) {
					answer.push_back(v);
				}
			}

			if(curSum < 0)	left++;
			else right--;
		}
	}   
	return answer;
}