int Solution::nTriang(vector<int> &A) {
	long long MOD = 1000000007;
	int answer = 0;

	std::sort (A.begin(), A.end());
	int n = A.size();
	for(int i=0 ; i<n-2 ; i++){
		int k = i+2;
		for(int j=i+1 ; j<n ; j++) {

		while (k < n && A[i] + A[j] > A[k]) 
           ++k; 

		answer = ( (long long)answer + (k-j-1) ) % MOD;
	}
}

	return answer;   
}
