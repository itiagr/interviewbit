vector<int> Solution::intersect(const vector<int> &A, const vector<int> &B) {

	int numA = A.size(), numB = B.size();
	int indexA = 0, indexB = 0;
	vector<int> intersections;

	while (indexA < numA && indexB < numB) {
		if (A[indexA] == B[indexB]) {
			intersections.push_back(A[indexA]);
			indexA++;
			indexB++;
		} else if (A[indexA] < B[indexB]) {
			indexA++;
		} else {
			indexB++;
		}
	} 
	return intersections;
}
