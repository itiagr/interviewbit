int Solution::removeDuplicates(vector<int> &A) {
    int n = A.size();

    if(n==0 || n==1 || n==2) return n;
    
    int firstP = 2, secondP = 2;
    int lastIndexUpdated = -1, valueAtLastIndexUpdated = -1;

    while(firstP < n){
        if(lastIndexUpdated == firstP-2) {
            if(A[firstP] != valueAtLastIndexUpdated) {
                lastIndexUpdated = secondP;
                valueAtLastIndexUpdated = A[secondP];
                A[secondP] = A[firstP];
                secondP++;
            }
        } else if(A[firstP] != A[firstP-2]) {
            lastIndexUpdated = secondP;
            valueAtLastIndexUpdated = A[secondP];
            A[secondP] = A[firstP];
            secondP++;
        }
        firstP++;
    }

    while(n != secondP) {
        A.pop_back();
        n--;
    }

    return secondP;
}
