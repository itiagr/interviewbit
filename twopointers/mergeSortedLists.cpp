void Solution::merge(vector<int> &A, vector<int> &B) {
    
	int numA = A.size(), numB = B.size();
	int indA = 0, indB = 0;
	while(indB < numB){
		if(indA < numA){
			if(A[indA] <= B[indB]){
				indA++;
			} else {
				A.insert(A.begin() + indA, B[indB]);
				numA++;
				indB++;
			}
		} else {
			A.push_back(B[indB]);
			indB++;
		}
	}

}
