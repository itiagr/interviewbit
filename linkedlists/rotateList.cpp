/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
ListNode* Solution::rotateRight(ListNode* A, int B) {
	ListNode* head = A;

	int n = 0;
	ListNode* lastNode = NULL;
	ListNode* nodeAtB = A;

	while(head != NULL) {
		n++;
		lastNode = head;
		head = head->next;
	}

	B = B % n;
	head = A;

	int counter = 0;

	while(counter < n - B - 1) {
		nodeAtB = nodeAtB->next;
		counter++;
	}

	lastNode->next = head;
	head = nodeAtB->next;
	nodeAtB->next = NULL;

	return head;
}
