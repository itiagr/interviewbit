/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
ListNode* Solution::deleteDuplicates(ListNode* A) {

	if(A == NULL)	return A;

	ListNode* head = A;

	ListNode* fastPtr = head->next;

	while(fastPtr != NULL) {
		if(head->val != fastPtr->val) {
			head->next = fastPtr;
			head = fastPtr;
		}

		fastPtr = fastPtr->next;
	}

	head->next = NULL;

	return A;
}
