/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

ListNode* Solution::addTwoNumbers(ListNode* A, ListNode* B) {
	ListNode* sum = NULL;
	ListNode* ptrA = A;
	ListNode* ptrB = B;
	ListNode* ptrS = sum;
	int carry = 0;
	
	while(ptrA != NULL && ptrB != NULL) {
		int s = ptrA->val + ptrB->val + carry;

		ListNode* sumDigit = new ListNode(s % 10);
		carry = s / 10;

		if(ptrS == NULL) {
			sum = sumDigit;
			ptrS = sum;
		} else {
			ptrS->next = sumDigit;
			ptrS = ptrS->next;
		}

		ptrA = ptrA->next;
		ptrB = ptrB->next;
	}

	while(ptrA != NULL) {
		int s = ptrA->val + carry;

		ListNode* sumDigit = new ListNode(s % 10);
		carry = s / 10;

		if(ptrS == NULL) {
			sum = sumDigit;
			ptrS = sum;
		} else {
			ptrS->next = sumDigit;
			ptrS = ptrS->next;
		}

		ptrA = ptrA->next;
	}

	while(ptrB != NULL) {
		int s = ptrB->val + carry;

		ListNode* sumDigit = new ListNode(s % 10);
		carry = s / 10;

		if(ptrS == NULL) {
			sum = sumDigit;
			ptrS = sum;
		} else {
			ptrS->next = sumDigit;
			ptrS = ptrS->next;
		}

		ptrB = ptrB->next;
	}

	if(carry) {
		ListNode* sumDigit = new ListNode(carry);

		if(ptrS == NULL) {
			sum = sumDigit;
			ptrS = sum;
		} else {
			ptrS->next = sumDigit;
			ptrS = ptrS->next;
		}
	}

	return sum;
}
