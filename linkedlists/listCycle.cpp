/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
ListNode* Solution::detectCycle(ListNode* head) {
    ListNode* slowPtr = head;
    ListNode* fastPtr = head;

    bool cycleFound = false;
    while(slowPtr != NULL && fastPtr != NULL && fastPtr->next != NULL) {
    	slowPtr = slowPtr->next;
    	fastPtr = fastPtr->next->next;

    	if(slowPtr == fastPtr) {
    		cycleFound = true;
    		break;
    	}
    }

    if(!cycleFound) return NULL;

    slowPtr = head;

    while(slowPtr != fastPtr) {
    	slowPtr = slowPtr->next;
    	fastPtr = fastPtr->next;
    }

    return slowPtr;
}
