/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

ListNode* reverseList(ListNode* head) {
	if(head == NULL)	return head;

	ListNode* previous = head;
	ListNode* current = previous->next;
	while(previous != NULL && current != NULL) {
		//cout<<"previous "<<previous->val<<" current "<<current->val<<endl;
		previous->next = current->next;
		current->next = head;
		head = current;
		current = previous->next;
	}
	return head;
}

ListNode* Solution::reorderList(ListNode* A) {
	int n = 0;

	ListNode* ptr = A;
	while(ptr != NULL) {
		n++;
		ptr = ptr->next;
	}

	if(n <= 2)	return A;

	int mid = (n + 1) / 2;
	ptr = A;

	//get to the middle
	for(int i = 1 ; i < mid ; i++) {
		ptr = ptr->next;
	}

	//reverse 2nd half of list
	ListNode* secondHalfHead = reverseList(ptr->next);

	//update first half to point to NULL
	ptr->next = NULL;
	
	ptr = A;
	while(secondHalfHead != NULL) {
		ListNode* tmp = secondHalfHead;
		secondHalfHead = secondHalfHead->next;

		tmp->next = ptr->next;
		ptr->next = tmp;
		ptr = tmp->next;
	}
	return A;
}
