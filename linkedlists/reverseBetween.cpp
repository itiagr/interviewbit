/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

ListNode* reverseKList(ListNode* head, int K) {
	if(head == NULL)	return head;

	ListNode* previous = head;
	ListNode* current = previous->next;
	while(previous != NULL && current != NULL && (K > 1)) {
		//cout<<"previous "<<previous->val<<" current "<<current->val<<endl;
		previous->next = current->next;
		current->next = head;
		head = current;
		current = previous->next;
		K--;
	}
	return head;
}

ListNode* Solution::reverseBetween(ListNode* A, int B, int C) {

	if(B == 1) {
		return reverseKList(A,C);
	}

	ListNode* head = A;
	ListNode* prev = NULL;
	for(int i = 1 ; i < B ; i++) {
		prev = head;
		head = head->next;
	}

	prev->next = reverseKList(head, C - B + 1);
	return A;
}
