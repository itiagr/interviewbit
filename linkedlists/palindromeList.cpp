/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
int Solution::lPalin(ListNode* A) {
	int n = 0;
	ListNode* head = A;

	//count number of nodes
	while(head != NULL) {
		n++;
		head = head->next;
	}

	if(n==0 || n==1)	return 1;

	head = A;
	ListNode* headAhead = A;

	int counter = (n - 1) / 2;

	//got to middle-1 node
	while(counter) {
		headAhead = headAhead->next;
		counter--;
	}	

	//reverse the 2nd half of linked list
	ListNode* pointer = (headAhead->next)->next;
	ListNode* previous = headAhead->next;
	while(previous != NULL && pointer != NULL) {
		previous->next = pointer->next;
		pointer->next = headAhead->next;
		headAhead->next = pointer;
		pointer = previous->next;
	}

	headAhead = headAhead->next;

	//compare the two halves
	while(headAhead != NULL) {
		if(head->val != headAhead->val) {
			return 0;
		}

		headAhead = headAhead->next;
		head = head->next;
	}

	return 1;
}
