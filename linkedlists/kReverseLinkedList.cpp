/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

// void prineLinkedList(ListNode* A) {

// 	while(A != NULL){
// 		cout<<" "<<A->val<<" ->";
// 		A = A->next;
// 	}
// 	cout<<endl;

// }

ListNode* reverseKList(ListNode* head, int K) {
	if(head == NULL)	return head;

	ListNode* previous = head;
	ListNode* current = previous->next;
	while(previous != NULL && current != NULL && (K > 1)) {
		//cout<<"previous "<<previous->val<<" current "<<current->val<<endl;
		previous->next = current->next;
		current->next = head;
		head = current;
		current = previous->next;
		K--;
	}
	return head;
}


ListNode* Solution::reverseList(ListNode* A, int B) {

	if(B < 2)	return A;

	A = reverseKList(A, B);
	ListNode* headAhead = A;
	ListNode* previous = NULL;
	//prineLinkedList(A);

	while(headAhead != NULL && headAhead->next != NULL) {
		for(int i = 0 ; i < B ; i++) {
			previous = headAhead;
			headAhead = headAhead->next;
		}
		
		headAhead = reverseKList(headAhead, B);

		previous->next = headAhead;
	}

	return A;
}
