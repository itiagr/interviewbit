/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
ListNode* Solution::partition(ListNode* head, int B) {

	ListNode* prev = head;
	ListNode* cur = head;

	//find first element lesser than B and put it on head

	while(cur != NULL && cur->val >= B) {
		prev = cur;
		cur = cur->next;
	}

	if(cur != NULL && cur != head) {
		prev->next = cur->next;
		cur->next = head;
		head = cur;
	}

	prev = head;
	cur = head;

	//find first element greater than or equal to B
	while(cur != NULL && cur->val < B) {
		prev = cur;
		cur = cur->next;
	}

	ListNode* nodeWithBAtNext = prev;

	while(cur != NULL) {
		if(cur->val < B) {
			prev->next = cur->next;
			cur->next = nodeWithBAtNext->next;
			nodeWithBAtNext->next = cur;
			nodeWithBAtNext = nodeWithBAtNext->next;
			cur = prev->next;
		} else {
			prev = cur;
			cur = cur->next;
		}
	}
	return head;
}
