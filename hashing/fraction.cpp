string Solution::fractionToDecimal(int A, int B) {

	long long num = A, den = B;

	if(num == 0) return "0";

	bool neg = false;

	if(num < 0) {
		neg = true;
		num = num / -1;
	}

	if(den < 0) {
		neg = !neg;

		den *= -1;
	}

	string solution = "";
	if(neg) solution += "-";

	long long dividend = num / den;
	num = num % den;
	solution += to_string(dividend);

	if(num == 0)	return solution;

	solution += ".";

	map<long long, int> hashmap;
	bool repeat = false;
	int positionOfRepitition;
	while(num) {
		num = num*10;

		if(hashmap.find(num) != hashmap.end()) {
			repeat = true;
			positionOfRepitition = hashmap[num];
			break;
		} else {
			hashmap[num] = solution.size();
		}

		dividend = num / den;
		num = num % den;
		solution += to_string(dividend);
	}

	if(repeat) {
		solution.insert(positionOfRepitition, "(");
		solution += ")";
	}

	return solution;
}
