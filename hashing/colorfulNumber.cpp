int Solution::colorful(int n) {
	vector<int> subsSequences;

	while(n) {
		subsSequences.push_back(n % 10);
		n = n / 10;
	}

	int sz = subsSequences.size();

	vector<int> hash;

	for(int i = 0 ; i < sz ; i++) {
		int product = subsSequences[i];

		if(find(hash.begin(), hash.end(), product) != hash.end()) {
			return 0;
		}

		hash.push_back(product);

		for(int j = i + 1 ; j < sz ; j++) {
			product *= subsSequences[j];
			if(find(hash.begin(), hash.end(), product) != hash.end()) {
				return 0;
			}
			hash.push_back(product);O
		}
	}

	return 1;
}
