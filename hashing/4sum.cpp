vector<vector<int> > Solution::fourSum(vector<int> &A, int B) {
	vector<vector<int> > ans;
	vector<int> temp;
	int n = A.size(), sum;
	sort(A.begin(), A.end());

	for(int i = 0 ; i < n - 3 ; i++) {
		for(int j = i + 1 ; j < n - 2 ; j++) {
			int left = j + 1, right = n - 1;

			while(left < right) {
				sum = A[i] + A[j] + A[left] + A[right];

				if(B == sum) {
					temp.push_back(A[i]);
					temp.push_back(A[j]);
					temp.push_back(A[left]);
					temp.push_back(A[right]);
					ans.push_back(temp);
					temp.erase(temp.begin(), temp.end());

					while(A[right] == A[right - 1] && right > 0) {
						right--;
					}

					while(A[left] == A[left + 1] && left < n - 1) {
						left++;
					}

					left++;
					right--;
				} else if(sum < B) {
					left++;
				} else {
					right--;
				}
			}

			while(A[j] == A[j + 1]) {
				j++;
			}
		}

		while(A[i] == A[i + 1]) {
			i++;
		}
	}

	return ans;

}
