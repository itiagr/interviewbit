struct Pair{
    int left;
    int right;
};

vector<int> Solution::lszero(vector<int> &A) {
	int n = A.size();

	map<int, Pair > map;

	int s = 0;
	for(int i = 0 ; i < n ; i++) {
		s += A[i];
		if(map.find(s) != map.end()) {
			map[s].right = i;
		} else {
			map[s].left = i;
			map[s].right = -1;
		}
	}

	for(auto it = map.begin(); it != map.end(); ++it) {
		//cout<<it->first<<"=>("<<it->second.left<<","<<it->second.right<<")\n";
	}

	int left = map.size(), right = 0;

	for(auto it = map.begin(); it != map.end(); ++it) {
		if(it->first == 0) {
			int possibleLeft = -1;
			int possibleRight = (it->second.right != -1) ? it->second.right : it->second.left;

			if( (right - left) < (possibleRight - possibleLeft) ||
				( (right - left) == (possibleRight - possibleLeft) &&
					left > possibleLeft)
				) {
				left = possibleLeft;
				right = possibleRight;
			} 
		} else if(it->second.right != -1) {
			if( (right - left) < (it->second.right - it->second.left) ||
				( (right - left) == (it->second.right - it->second.left) &&
					left > it->second.left)
				) {

				left = it->second.left;
				right = it->second.right;
			}
		}
	}

	vector<int> ans;
	for(int i = left + 1 ; i <= right ; i++) {
		ans.push_back(A[i]);
	}

	return ans;
}
