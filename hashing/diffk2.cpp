int Solution::diffPossible(const vector<int> &A, int B) {
	map<int, int> hashmap;
	int n = A.size();

	for(int i = 0 ; i < n ; i++) {
		for(int j = i + 1 ; j < n ; j++) {
			if((A[i] - A[j] == B) || (A[j] - A[i] == B)) {
				return 1;
			}
		}
	}

	return 0;
}
