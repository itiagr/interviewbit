bool isLexicographicallySmaller(vector<int> ans, vector<int> possibleAns) {
	if(ans.empty())	return true;

	return (  
		( possibleAns[0] < ans[0] ) ||
		( possibleAns[0] == ans[0] && possibleAns[1] < ans[1] ) || 
		( possibleAns[0] == ans[0] && possibleAns[1] == ans[1] && possibleAns[2] < ans[2] ) ||
		( possibleAns[0] == ans[0] && possibleAns[1] == ans[1] && possibleAns[2] == ans[2] && possibleAns[3] < ans[3] )
	);
}

vector<int> Solution::equal(vector<int> &A) {
	int n = A.size();
	map<int, pair<int, int> > sumHash;

	vector<int> ans;

	for(int i = 0 ; i < n ; i++) {
		for(int j = i + 1 ; j < n ; j++) {
			int sum = A[i] + A[j];
			if(sumHash.find(sum) == sumHash.end()) {
				sumHash[sum] = make_pair(i,j);
				continue;
			}
			
			int a = sumHash[sum].first;
			int b = sumHash[sum].second;
			int c = i;
			int d = j;

			vector<int> tmp{a, b, c, d};

			if(a < b && c < d && a < c && b != d && b != c && isLexicographicallySmaller(ans, tmp)) {
				ans = tmp;
			}
		}
	}

	return ans;
}
