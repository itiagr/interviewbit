/**
 * Definition for singly-linked list.
 * struct RandomListNode {
 *     int label;
 *     RandomListNode *next, *random;
 *     RandomListNode(int x) : label(x), next(NULL), random(NULL) {}
 * };
 */
RandomListNode* Solution::copyRandomList(RandomListNode* originalList) {

	RandomListNode* copyList = NULL;
	RandomListNode* originalHead = originalList;

	map<RandomListNode* , RandomListNode*> nodeHashMap;

	while(originalHead != NULL) {
		RandomListNode* newNode = new RandomListNode(originalHead->label);
		nodeHashMap.insert({originalHead, newNode});
		if(copyList == NULL) {
			copyList = newNode;
		}

		originalHead = originalHead->next;
	}

	originalHead = originalList;
	//cout<<originalHead->label;
	while(originalHead != NULL) {
		RandomListNode* copyNode = nodeHashMap[originalHead];

		if(originalHead->next != NULL){
			copyNode->next = nodeHashMap[originalHead->next];
		}

		if(originalHead->random != NULL){
			copyNode->random = nodeHashMap[originalHead->random];
		}
		originalHead = originalHead->next;
	}

	return copyList;
}
