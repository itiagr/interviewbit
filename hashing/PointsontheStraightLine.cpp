int greatestCommonDivisor(int m, int n) {
	if(m == 0 && n == 0)	return 1;
	if(m == 0)	return n;
	if(n == 0)	return m;
	if(m == n)	return m;

	if(m > n)
		return greatestCommonDivisor(m % n , n);
	return greatestCommonDivisor(m, n % m);
}

int Solution::maxPoints(vector<int> &A, vector<int> &B) {
    int n = A.size();

    if(n < 2)	return n;

    map< double, int> slopesMap;

    int ans = 0;

    for(int i = 0 ; i < n ; i++) {
    	int duplicate = 1, vertical = 0;
    	for(int j = i + 1 ; j < n ; j++) {

    		if(A[i] == A[j]) {
    			if(B[i] == B[j]) {
    				duplicate++;
    			} else {
    				vertical++;
    			}
    			continue;
    		}

    		double slope = 0.0;
            double x = A[j] - A[i];
            double y = B[j] - B[i];
            if(B[j] != B[i]){
                slope = (1.0 * (y/x));
            }
    		if(slopesMap.find(slope) != slopesMap.end()) {
    			slopesMap[slope]++;
    		} else {
    			slopesMap.insert({slope, 1});
    		}
    	}

    	auto it = slopesMap.begin();
        while(it != slopesMap.end()){
            ans = max(ans, it->second + duplicate);
            it++;
        }

        ans = max(ans, vertical + duplicate);

    	slopesMap.clear();
    }
    return ans;
}
