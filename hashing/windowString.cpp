struct countInfo {
	int required, found;
};

string Solution::minWindow(string S, string T) {
	map<char, countInfo> countInfoMap;

	int n = T.length();
	for(int i = 0 ; i < n ; i++) {
		if(countInfoMap.find(T[i]) == countInfoMap.end()) {
			countInfoMap[T[i]].required = 1;
			countInfoMap[T[i]].found = 0;
		} else {
			countInfoMap[T[i]].required++;
		}
	}

	int wLeft = 0, wRight = 0, curLen = 0, lengthS = S.length();
	int minIndex = 0, maxIndex = 0, minLength = INT_MAX;
	while(1) {
		if(curLen < n){
			if(wRight == lengthS){
				break;
			}

			if(countInfoMap.find(S[wRight]) != countInfoMap.end()) {
				countInfoMap[S[wRight]].found++;

				if(countInfoMap[S[wRight]].required >= countInfoMap[S[wRight]].found) {
					curLen++;
				}
			}
			wRight++;
		} else if(curLen == n) {
			if(wRight - wLeft < minLength) {
				maxIndex = wRight;
				minIndex = wLeft;
				minLength = maxIndex - minIndex;
			}

			if(countInfoMap.find(S[wLeft]) != countInfoMap.end()) {
				countInfoMap[S[wLeft]].found--;

				if(countInfoMap[S[wLeft]].required > countInfoMap[S[wLeft]].found) {
					curLen--;
				}
			}
			wLeft++;
		}
	}

	string ans = "";

	for(int i = minIndex ; i < maxIndex ; i++) {
		ans += S[i];
	}

	return ans;
}
