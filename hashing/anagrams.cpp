bool isAnagram(vector<int> x, vector<int> y) {
	for(int i = 0 ; i < 26 ; i++) {
		if(x[i] != y[i])	return false;
	}

	return true;
}

vector<vector<int> > Solution::anagrams(const vector<string> &A) {
	int n = A.size();

	vector<vector<int> > hash;
	for(int i = 0 ; i < n ; i++) {
		vector<int>tmp(26, 0);
		int l = A[i].length();
		for(int j = 0 ; j < l ; j++) {
			tmp[A[i][j] - 'a']++;
		}
		hash.push_back(tmp);
	}

	vector<bool> anagramFound(n, false);

	vector<vector<int> > anagrams;
	for(int i = 0 ; i < n ; i++) {
		if(anagramFound[i])	continue;
		vector<int> anagramForI{i + 1};
		for(int j = i + 1; j < n ; j++) {
			if(anagramFound[j])	continue;
			if(isAnagram(hash[i], hash[j])) {
				anagramForI.push_back(j + 1);
				anagramFound[j] = true;
			}
		}
		anagrams.push_back(anagramForI);
	}

	return anagrams;
}
