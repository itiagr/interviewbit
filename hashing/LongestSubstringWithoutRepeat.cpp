int Solution::lengthOfLongestSubstring(string A) {
	map<char,int> charFound;

	int n = A.length();
	int ans = 0, lengthWithoutRepeat = 0, startsAt = 0;

	for(int i = 0 ; i < n ; i++) {
		if(charFound.find(A[i]) == charFound.end()) {
			lengthWithoutRepeat++;
			charFound[A[i]] = i;
		} else {
			ans = max(ans, lengthWithoutRepeat);
			lengthWithoutRepeat = i - charFound[A[i]];
			int j = startsAt;
			startsAt = charFound[A[i]] + 1;

			for( ; j <= charFound[A[i]] ; j++){
				charFound.erase(A[j]);
			}

			charFound[A[i]] = i;
		}
	}

	return ans = max(ans, lengthWithoutRepeat);
}
