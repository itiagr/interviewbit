const int NUM = 9;

bool isValid(const vector<string> &sudoku, int row, int col) {
	//check for valid row & col
	char elem = sudoku[row][col];
	for(int i = 0 ; i < NUM ; i++) {
		if((sudoku[i][col] == elem && i != row ) || (sudoku[row][i] == elem && i != col))	return false;
	}

	//check for 3x3 grid

	int gridStartRow = (row / 3) * 3;
	int gridStartCol = (col / 3) * 3;

	for(int i = gridStartRow ; i < gridStartRow + 3 ; i++) {
		for(int j = gridStartCol ; j < gridStartCol + 3 ; j++) {
			if( (i != row || j != col) && sudoku[i][j] == elem)	return false;
		}
	}

	return true;
}

int Solution::isValidSudoku(const vector<string> &A) {

	for(int row = 0 ; row < NUM ; row++) {
		for(int col = 0 ; col < NUM ; col++) {
			if(A[row][col] != '.' && !isValid(A, row, col)) {
				return 0;
			}
		}
	}

	return 1;
}
