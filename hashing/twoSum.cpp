vector<int> Solution::twoSum(const vector<int> &A, int B) {
	map<int, int> hashmap;
	vector<int> ans;
	int n = A.size();

	for(int i = 0 ; i < n ; i++) {
		if(hashmap.find(B - A[i]) != hashmap.end() && hashmap[B - A[i]] != i) {
			if(ans.empty()) {
				ans.push_back(hashmap[B - A[i]]);
				ans.push_back(i);
			} else if(ans[1] > i || (ans[1] == i && ans[0] > hashmap[B - A[i]])) {
				ans[0] = hashmap[B - A[i]];
				ans[1] = i;
			}
		}

		if(hashmap.find(A[i]) == hashmap.end()) {
			hashmap[A[i]] = i;
		}
	}

	if(!ans.empty()){
		ans[0]++;
		ans[1]++;
	}

	return ans;
}
