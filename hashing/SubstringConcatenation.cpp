vector<int> Solution::findSubstring(string A, const vector<string> &B) {
	
	int l = A.length(), numString = B.size();

	if(l == 0 || numString == 0)	return vector<int>();

	vector<int> ans;
	int lenSubString = B[0].size();

	map<string, int> subStringCount;

	for(int i = 0; i< numString ; i++) {
		if(subStringCount.find(B[i]) == subStringCount.end()) {
			subStringCount[B[i]] = 1;
		} else {
			subStringCount[B[i]]++;
		}
	}

	for(int i = 0 ; i < l ; i++) {
		int length = 0;

		map<string, int> tempSubStringCount;
		int j = 0;
		for( ; j < numString ; j++) {
			string str = A.substr(i + lenSubString*j, lenSubString);

			if(subStringCount.find(str) == subStringCount.end()) {
				break;
			}

			if(tempSubStringCount.find(str) == tempSubStringCount.end()) {
				tempSubStringCount[str] = 1;
			} else {
				tempSubStringCount[str]++;
			}

			if(tempSubStringCount[str] > subStringCount[str]) {
				break;
			}
		}

		if(j == numString) {
			ans.push_back(i);
		}

	}

	return ans;
}
