/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

ListNode* reverseList(ListNode* head) {
	if(head == NULL)	return head;

	ListNode* previous = head;
	ListNode* current = previous->next;
	while(previous != NULL && current != NULL) {
		//cout<<"previous "<<previous->val<<" current "<<current->val<<endl;
		previous->next = current->next;
		current->next = head;
		head = current;
		current = previous->next;
	}
	return head;
}

ListNode* Solution::subtract(ListNode* A) {

	int n = 0;

	ListNode* head = A;
	while(head != NULL)	 {
		n++;
		head = head->next;
	}

	if(n < 2)	return A;

	int mid = n / 2;
	head = A;

	//get to the middle
	for(int i = 0 ; i < mid ; i++) {
		head = head->next;
	}

	//reverse 2nd half of list
	ListNode* secondHalfHead = NULL;
	if(n % 2) {
		secondHalfHead = reverseList(head->next);
	} else {
		secondHalfHead = reverseList(head);
	}

	ListNode* p1 = A;
	ListNode* p2 = secondHalfHead; 
	
	while(mid) {
		p1->val = p2->val - p1->val;

		p1 = p1->next;
		p2 = p2->next;
		mid--;
	}

	reverseList(secondHalfHead);
	return A;
}

