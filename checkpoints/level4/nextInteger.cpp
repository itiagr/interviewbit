vector<int> Solution::nextGreater(vector<int> &A) {
	int n = A.size();
	deque<int> dMax;

	vector<int> ans;

	for(int i = n - 1 ; i >= 0 ; i--) {

		while(!dMax.empty() && dMax.back() <= A[i]) {
			dMax.pop_back();
		}

		if(dMax.empty()) {
			ans.push_back(-1);
		} else {
			ans.push_back(dMax.back());
		}

		dMax.push_back(A[i]);
	}

	reverse(ans.begin(),ans.end());
	return ans;
}
