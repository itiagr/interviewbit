set<vector<int> > generatePermutations(vector<int> &currentPerm, int index, int n) {
	set<vector<int> > ans;
	if(index == n) {
		ans.insert(currentPerm);
		return ans;
	}

	set<vector<int> > tmp = generatePermutations(currentPerm, index + 1, n);
	ans.insert(tmp.begin(), tmp.end());

	for(int i = index + 1 ; i < n ; i++) {
		iter_swap(currentPerm.begin() + index, currentPerm.begin() + i);
		set<vector<int> > tmp = generatePermutations(currentPerm, index + 1, n);
		ans.insert(tmp.begin(), tmp.end());
		iter_swap(currentPerm.begin() + index, currentPerm.begin() + i);
	}
	return ans;
}

vector<vector<int> > Solution::permute(vector<int> &A) {
	set<vector<int> > ansSet = generatePermutations(A, 0, A.size());
	return vector<vector<int> >(ansSet.begin(), ansSet.end());
}
