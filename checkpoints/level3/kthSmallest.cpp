int Solution::kthsmallest(const vector<int> &A, int B) {
    int left = INT_MAX, right = INT_MIN;
    int n = A.size();

    for(int i = 0 ; i < n ; i++) {
        left = min(left, A[i]);
        right = max(right, A[i]);
    }

    while(left <= right) {
        int mid = (left + right) / 2;
        int countLess = 0, countEqual = 0;
        for (int i = 0; i < n; ++i) {
            if(A[i] == mid) {
                countEqual++;
            }

            if(A[i] < mid) {
                countLess++;
            }
        }

        if(countLess < B && countLess + countEqual >= B) {
            return mid;
        }
        if(countLess >= B) {
            right = mid - 1;
        } else {
            left = mid + 1;
        }
    }

    return -1;
}
