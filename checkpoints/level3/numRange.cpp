int Solution::numRange(vector<int> &A, int B, int C) {
	int n = A.size();
	int answer = 0, sum = 0;
	int firstP = 0, secondP = 0;

	while(firstP < n) {
		sum += A[secondP];

		if(B <= sum && sum <= C) {
			answer++;
			secondP++;
		} else if( sum < B) {
			secondP++;
		} else if( sum > C) {
			firstP++;
			secondP = firstP;
			sum = 0;
		}

		if(secondP == n) {
			firstP++;
			secondP = firstP;
			sum = 0;
		}
	}

	return answer;
}
