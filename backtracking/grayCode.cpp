vector<int> generateGrayCode(int &code, int n) {
	if(n == 0) return vector<int>{code};

	vector<int> ans;
	vector<int> tmp = generateGrayCode(code, n - 1);
	ans.insert(ans.end(), tmp.begin(), tmp.end());

	code = code ^ (1 << (n - 1) );
	tmp = generateGrayCode(code, n - 1);
	ans.insert(ans.end(), tmp.begin(), tmp.end());

	return ans;
}


vector<int> Solution::grayCode(int A) {
	int code = 0;
    return generateGrayCode(code, A);
}
