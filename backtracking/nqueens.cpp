bool isValidPlacement(vector<string> &board, int row, int col, int n) {

	for(int i = 0 ; i < col ; i++) {
		if(board[row][i] == 'Q')	return false;
	}

	for(int i = 1 ; i < n ; i++) {
		if(row - i >= 0 && col - i >= 0 && board[row - i][col - i] == 'Q')	return false;
		if(row - i >= 0 && col + i < n && board[row - i][col + i] == 'Q')	return false;
		if(row + i < n && col - i >= 0 && board[row + i][col - i] == 'Q')	return false;
		if(row + i < n && col + i < n && board[row + i][col + i] == 'Q')	return false;
	}
	//cout<<"yes "<<row<<","<<col<<endl;
	return true;
}

vector<vector<string> > solve(vector<string> &board, int col, int n) {

	vector<vector<string> > boardConfigs;

	if(col == n) {
		boardConfigs.push_back(board);
		return boardConfigs;
	}

	for(int row = 0 ; row < n ; row++) {
		if(isValidPlacement(board, row, col, n)) {
			board[row][col] = 'Q';
			vector<vector<string> > tmp = solve(board, col + 1, n);
			boardConfigs.insert(boardConfigs.end(), tmp.begin(), tmp.end());
			board[row][col] = '.';
		}
	}

	return boardConfigs;
}

vector<vector<string> > Solution::solveNQueens(int n) {
	string boardRow(n, '.');
    vector<string> board(n, boardRow);

    return solve(board, 0, n);
}
