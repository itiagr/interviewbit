bool palindrome(string A, int start, int end) {
	while(start <= end) {
		if(A[start] != A[end]) {
			return false;
		}

		start++;
		end--;
	}

	return true;
}

vector<vector<string> > getPartition(int start, int end, string A, int n) {
	vector<vector<string> > ans;
	if(start >= n || end >= n) return ans;

	//cout<<"string - "<<A.substr(start, end - start + 1)<<endl;

	if(palindrome(A, start, end)) {
		//cout<<"palindrome - "<<A.substr(start, end - start + 1)<<endl;
		vector<vector<string> > ansForNextString = getPartition(end + 1, end + 1, A, n);

		int l = ansForNextString.size();

		if(l == 0) {
			vector<string> tmp;
			tmp.push_back(A.substr(start, end - start + 1));
			ans.push_back(tmp);
		} else {
			for(int i = 0 ; i < l ; i++) {
				vector<string> tmp;
				tmp.push_back(A.substr(start, end - start + 1));
				tmp.insert(tmp.end(), ansForNextString[i].begin(), ansForNextString[i].end());
				ans.push_back(tmp);
			}
		}
	}

	vector<vector<string> > ansWithCurrentString = getPartition(start, end + 1, A, n);

	int l = ansWithCurrentString.size();

	for(int i = 0 ; i < l ; i++) {
		ans.push_back(ansWithCurrentString[i]);
	}

	//cout<<ans.size()<<endl;

	return ans;
}

vector<vector<string> > Solution::partition(string A) {
    return getPartition(0, 0, A, A.length());
}
