int getNumberOfPerm(int n) {
    
    if(n > 12)  return INT_MAX;
    
    int fact = 1;

    for(int i = 2 ; i <= n; i++) {
        fact *= i;
    }
    return fact;
}

string generateKthPermutation(vector<int> &num, int k) {
    int n = num.size();

    if(n == 0)    return "";
    if(k > getNumberOfPerm(n))    return "";    //unreachable condition

    int numPerm = getNumberOfPerm(n - 1);
    int pos = ( k / numPerm );
    string permutation = to_string(num[pos]);
    num.erase(num.begin() + pos);
    return permutation + generateKthPermutation(num, k % numPerm);
}

string Solution::getPermutation(int n, int k) {
    vector<int> num;

    for (int i = 1; i <= n; ++i)
    {
        num.push_back(i);
    }

    return generateKthPermutation(num, k - 1);
}
