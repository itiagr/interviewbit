const int NUM = 9;

bool isValid(vector<vector<char> > &sudoku, int row, int col) {
	//check for valid row & col
	char elem = sudoku[row][col];
	for(int i = 0 ; i < NUM ; i++) {
		if((sudoku[i][col] == elem && i != row ) || (sudoku[row][i] == elem && i != col))	return false;
	}

	//check for 3x3 grid

	int gridStartRow = (row / 3) * 3;
	int gridStartCol = (col / 3) * 3;

	for(int i = gridStartRow ; i < gridStartRow + 3 ; i++) {
		for(int j = gridStartCol ; j < gridStartCol + 3 ; j++) {
			if( (i != row || j != col) && sudoku[i][j] == elem)	return false;
		}
	}

	return true;
}

bool sudokuHelper(vector<vector<char> > &sudoku, int row, int col) {
	if(row == NUM)	return true;

	int nextRow = (col == NUM - 1) ? row + 1 : row;
	int nextCol = (col == NUM - 1) ? 0 : col + 1;

	if(sudoku[row][col] != '.') {
		return isValid(sudoku, row, col) && sudokuHelper(sudoku, nextRow, nextCol);
	}

	for(int i = 1 ; i <= NUM ; i++) {
		sudoku[row][col] = '0' + i;

		if(isValid(sudoku, row, col) && sudokuHelper(sudoku, nextRow, nextCol)) {
			return true;
		}
	}

	sudoku[row][col] = '.';
	return false;
}


void Solution::solveSudoku(vector<vector<char> > &A) {
    sudokuHelper(A, 0 , 0);
}
