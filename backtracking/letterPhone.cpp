vector<string> findCombinations(vector<vector<char> > &digitToLetterMap, string letterCombo, string digits, int index, int n) {
	if(index == n){
		return vector<string>{letterCombo};
	}

	int currentDigit = digits[index] - '0';

	vector<string> combinations;
	vector<char> charsOnDigit = digitToLetterMap[currentDigit];

	for(int i = 0 ; i < charsOnDigit.size() ; i++) {
		vector<string> moreCombinations = findCombinations(digitToLetterMap, letterCombo + charsOnDigit[i], digits, index + 1, n);
		combinations.insert(combinations.end(), moreCombinations.begin(), moreCombinations.end());
	}

	return combinations;
}

vector<string> Solution::letterCombinations(string A) {
	vector<vector<char> > digitToLetterMap;

	digitToLetterMap.push_back(vector<char>{'0'});
	digitToLetterMap.push_back(vector<char>{'1'});
	digitToLetterMap.push_back(vector<char>{'a', 'b', 'c'});
	digitToLetterMap.push_back(vector<char>{'d', 'e', 'f'});
	digitToLetterMap.push_back(vector<char>{'g', 'h', 'i'});
	digitToLetterMap.push_back(vector<char>{'j', 'k', 'l'});
	digitToLetterMap.push_back(vector<char>{'m', 'n', 'o'});
	digitToLetterMap.push_back(vector<char>{'p', 'q', 'r', 's'});
	digitToLetterMap.push_back(vector<char>{'t', 'u', 'v'});
	digitToLetterMap.push_back(vector<char>{'w', 'x', 'y', 'z'});

	return findCombinations(digitToLetterMap, "", A, 0, A.length());

}
