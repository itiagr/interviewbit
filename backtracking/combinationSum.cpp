vector<vector<int> > findCombinations(vector<int> row, int sum, vector<int> &A, int target, int index, int n) {
	vector<vector<int> > combinations;
	if(index == n || sum > target)	return combinations;

	if(sum == target) {
		combinations.push_back(row);
		return combinations;
	}

	row.push_back(A[index]);

	vector<vector<int> > moreCombinations = findCombinations(row, sum + A[index], A, target, index, n);
	combinations.insert(combinations.begin(), moreCombinations.begin(), moreCombinations.end());

	row.pop_back();
	moreCombinations = findCombinations(row, sum, A, target, index + 1, n);
	combinations.insert(combinations.begin(), moreCombinations.begin(), moreCombinations.end());
	return combinations;
}

vector<vector<int> > Solution::combinationSum(vector<int> &A, int B) {
	sort(A.begin(), A.end());
	int n = A.size();

	vector<int> uniqueA;
	uniqueA.push_back(A[0]);
	for(int i = 1 ; i < n ; i++) {
		if(A[i] != A[i-1]) {
			uniqueA.push_back(A[i]);
		}
	}
	
	n = uniqueA.size();

	vector<vector<int> > combinations = findCombinations(vector<int>(), 0, uniqueA, B, 0, n);
	sort(combinations.begin(), combinations.end());

	return combinations;
}
