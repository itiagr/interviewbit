vector<vector<int> > findSubSet(vector<vector<int> > ans, vector<int> &A, int index, int n) {
	if(index == n)	return ans;

	int nAns = ans.size();

	for(int i = 0 ; i < nAns ; i++) {
		vector<int> tmp = ans[i];
		tmp.push_back(A[index]);
		sort(tmp.begin(), tmp.end());
		ans.push_back(tmp);
	}

	return findSubSet(ans, A, index + 1, n);
}

vector<vector<int> > Solution::subsets(vector<int> &A) {
    vector<vector<int> > ans;
    vector<int> firstAns;
    ans.push_back(firstAns);

    int n = A.size();

    ans = findSubSet(ans, A, 0, n);

    sort(ans.begin(), ans.end());
    return ans;
}
