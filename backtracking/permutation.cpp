vector<vector<int> > generatePermutations(vector<int> &currentPerm, int index, int n) {
	vector<vector<int> > ans;
	if(index == n) {
		ans.push_back(currentPerm);
		return ans;
	}

	vector<vector<int> > tmp = generatePermutations(currentPerm, index + 1, n);
	ans.insert(ans.end(), tmp.begin(), tmp.end());

	for(int i = index + 1 ; i < n ; i++) {
		iter_swap(currentPerm.begin() + index, currentPerm.begin() + i);
		vector<vector<int> > tmp = generatePermutations(currentPerm, index + 1, n);
		ans.insert(ans.end(), tmp.begin(), tmp.end());
		iter_swap(currentPerm.begin() + index, currentPerm.begin() + i);
	}
	return ans;
}

vector<vector<int> > Solution::permute(vector<int> &A) {
	return generatePermutations(A, 0, A.size());
}
