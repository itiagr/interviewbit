vector<string> generate(string parenthesisStr, int numOpened, int numClosed, int n) {

	if(numOpened == n && numClosed == n) {
		return vector<string>{parenthesisStr};
	}

	if(numClosed >= n)	return vector<string>();

	vector<string> ans;
	if(numOpened < n) {
		vector<string> tmp = generate(parenthesisStr + "(", numOpened + 1, numClosed, n);
		ans.insert(ans.end(), tmp.begin(), tmp.end());
	}

	if(numClosed < n && numClosed < numOpened) {
		vector<string> tmp = generate(parenthesisStr + ")", numOpened, numClosed + 1, n);
		ans.insert(ans.end(), tmp.begin(), tmp.end());
	}
	return ans;
}

vector<string> Solution::generateParenthesis(int A) {
	return generate("", 0, 0, A);
}
