set<vector<int> > findSubSet(vector<int> row, vector<int> &A, int index, int n) {
	set<vector<int> > ans;
	ans.insert(row);
	if(index == n)	return ans;

	set<vector<int> > moreSubsets = findSubSet(row, A, index + 1, n);
	ans.insert(moreSubsets.begin(), moreSubsets.end());

	row.push_back(A[index]);
	moreSubsets = findSubSet(row, A, index + 1, n);
	ans.insert(moreSubsets.begin(), moreSubsets.end());

	return ans;
}

vector<vector<int> > Solution::subsetsWithDup(vector<int> &A) {

	int n = A.size();
	sort(A.begin(), A.end());

	set<vector<int> > setOfSubsets = findSubSet(vector<int>(), A, 0, n);

	vector<vector<int> > subsets(setOfSubsets.begin(), setOfSubsets.end());

	return subsets;
}
