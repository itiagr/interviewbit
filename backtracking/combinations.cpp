vector<vector<int> > findCombination(vector<vector<int> > allCombos, int index, int n, int k) {
	vector<vector<int> > ans;

	if(index > n)	return ans;

	int numCombos = allCombos.size();

	for(int i = 0 ; i < numCombos ; i++) {
		vector<int> tmp = allCombos[i];
		tmp.push_back(index);
		if(tmp.size() == k) {
			ans.push_back(tmp);
		} else {
			allCombos.push_back(tmp);
		}
	}

	vector<vector<int> > moreCombosAns =  findCombination(allCombos, index + 1, n, k);
	ans.insert(ans.end(), moreCombosAns.begin(), moreCombosAns.end());
	//cout<<"index "<<index<<",ans "<<ans.size()<<endl;
	return ans;
}

vector<vector<int> > Solution::combine(int A, int B) {
    vector<vector<int> > ans;
    ans.push_back(vector<int>());

    ans = findCombination(ans, 1, A, B);

    sort(ans.begin(), ans.end());
    return ans;
}
