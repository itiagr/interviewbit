set<vector<int> > findCombinations(vector<int> row, int sum, vector<int> &A, int target, int index, int n) {
	set<vector<int> > combinations;
	if(sum > target)	return combinations;

	if(sum == target) {
		combinations.insert(row);
		return combinations;
	}

	if(index == n)	return combinations;


	set<vector<int> > moreCombinations = findCombinations(row, sum , A, target, index + 1, n);
	combinations.insert(moreCombinations.begin(), moreCombinations.end());

	row.push_back(A[index]);
	moreCombinations = findCombinations(row, sum + A[index], A, target, index + 1, n);
	combinations.insert(moreCombinations.begin(), moreCombinations.end());
	return combinations;
}

vector<vector<int> > Solution::combinationSum(vector<int> &A, int B) {
    sort(A.begin(), A.end());
	int n = A.size();

	set<vector<int> > comboSet = findCombinations(vector<int>(), 0, A, B, 0, n);
	vector<vector<int> > combinations(comboSet.begin(), comboSet.end());
	sort(combinations.begin(), combinations.end());

	return combinations;
}
