int findRightMostPosition(const vector<int> &A, int start, int end, int elem) {
	while(start <= end){
		int mid = (start + end) / 2;

		if(A[mid] == elem){
			return max(mid , findRightMostPosition(A, mid+1, end, elem));
		}

		if(A[mid] > elem) {
			end = mid - 1;
		} else {
			start = mid + 1;
		}
	}

	return -1;
}

int findLeftMostPosition(const vector<int> &A, int start, int end, int elem) {

	while(start <= end){
		int mid = (start + end) / 2;

		if(A[mid] == elem){
			return min(mid , findLeftMostPosition(A, start, mid-1, elem));
		}

		if(A[mid] > elem) {
			end = mid - 1;
		} else {
			start = mid + 1;
		}
	}

	return INT_MAX;
}

vector<int> Solution::searchRange(const vector<int> &A, int B) {
	vector<int> ans;
    ans.push_back(-1); ans.push_back(-1);

    int leftMostPos = findLeftMostPosition(A, 0, A.size() - 1, B);
    if(leftMostPos == INT_MAX)	return ans;

    int rightMostPos = findRightMostPosition(A, 0, A.size() - 1, B);

    ans[0] = leftMostPos;
    ans[1] = rightMostPos;

    return ans;
}
