long long rightLimit(vector<int> &C, int n){
    long long ans = 0;
    for(int i=0; i<n; i++)   ans += C[i];
    return ans;
}

int leftLimit(vector<int> &C, int n){
    int ans = INT_MIN;
    for(int i=0; i<n; i++){
        if(C[i] > ans){
            ans = C[i];
        }
    }   
    return ans;
}

bool isAssignmentPossible(vector<int> &A , int numBooks , long long pagesToAssign, int numStudents){
    long long pagesAssignedToStudent = 0;
    int studentsRequired = 1;
    for(int i=0 ; i<numBooks ; i++) {
        if(pagesAssignedToStudent + A[i] > pagesToAssign) {
            studentsRequired++;
            pagesAssignedToStudent = A[i];
        } else {
            pagesAssignedToStudent += A[i];
        }
    }
    
    //cout<<pagesToAssign << " : " << (studentsRequired == numStudents) << endl ;
    return (studentsRequired <= numStudents);
}

int Solution::books(vector<int> &A, int B) {
    int numBooks = A.size();
    if (numBooks < B)   return -1;
    long long right = rightLimit(A , numBooks);
    long long left = leftLimit(A , numBooks);
    int ans = -1;

    while(left <= right) {
        long long pagesToAssign = (left + right) / 2;
        if(isAssignmentPossible(A , numBooks , pagesToAssign , B)){
            right = pagesToAssign - 1;
            ans = pagesToAssign;
        } else {
            left = pagesToAssign + 1;
        }
    }

    return ans;
}