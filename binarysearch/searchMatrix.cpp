
int Solution::searchMatrix(vector<vector<int> > &A, int B) {
    int rows = A.size();
    int cols = A[0].size();
    
    //find the row where B can exist
    
    int start = 0, end = rows - 1;
    int rowWithElement = -1;
    
    while(start <= end && rowWithElement == -1) {
        int possibleRowWithElement = (start + end) / 2;
        
        if(A[possibleRowWithElement][0] <= B 
            && A[possibleRowWithElement][cols - 1] >= B){
                rowWithElement = possibleRowWithElement;
            }
            
        if(A[possibleRowWithElement][0] > B){
            end = possibleRowWithElement - 1;
        } else {
            start = possibleRowWithElement + 1;
        }
    }
    
    if(rowWithElement == -1) return 0;

    //find the element in rowWithElement

    start = 0; end = cols - 1;

    while(start <= end){
        int possibleColWithElement = (start + end) / 2;
        if (A[rowWithElement][possibleColWithElement] == B) {
            return 1;
        } else if (A[rowWithElement][possibleColWithElement] > B) {
            end = possibleColWithElement - 1;
        } else {
            start = possibleColWithElement + 1;
        }
    }

    return 0;
}
