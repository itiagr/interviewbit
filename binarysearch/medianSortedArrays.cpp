double findMedian(const vector<int> &A, const vector<int> &B, int numA, int numB){
    
    int minIndex = 0, maxIndex = numA, i, j;
    double median;

    while (minIndex <= maxIndex) {
        int i = (minIndex + maxIndex) / 2;
        int j = ( (numA + numB + 1) / 2 ) - i;

        //cout<<"minIndex : "<<minIndex<<",maxIndex : "<<maxIndex<<endl;
        //cout<<"i : "<<i<<",j : "<<j<<endl;

        if ( i > 0 && j < numB && A[i-1] > B[j] ) {
            maxIndex = i - 1;
        } else if ( j > 0 && i < numA && A[i] < B[j-1] ) {
            minIndex = i + 1;
        } else {
            if (i == 0) {
                median = B[j-1];
            } else if (j == 0) {
                median = A[i-1];
            } else {
                median = max(A[i-1] , B[j-1]);
            }

	        if( (numA + numB) % 2) {
	        	return median;
            } else {
            	if(i == numA) {
            		return ( median + B[j] ) / 2.0;
            	} else if(j == numB) {
            		return ( median + A[i] ) / 2.0;
             	} else {
             		return ( median + min(A[i] , B[j]) ) / 2.0;
             	}
            }
        }
    }
}

double Solution::findMedianSortedArrays(const vector<int> &A, const vector<int> &B) {

    int numA = A.size();
    int numB = B.size();
    int n = numA + numB ;

     if(numA < numB){
         return findMedian(A, B, numA, numB);
     } else {
         return findMedian(B, A, numB, numA);
     }
}
