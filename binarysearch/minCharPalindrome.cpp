bool isPalindrome(string A, int endIndex){
    int startIndex = 0;
    while(startIndex <= endIndex){
    	if(A[startIndex] != A[endIndex]) return false;
    	startIndex++;
    	endIndex--;
    }
    return true;
}

int Solution::solve(string A) {
    int n = A.length();
    
    for(int i=0 ; i<n ; i++){
        if(isPalindrome(A, n-i-1)){
            return i;
        }
    }
    
    return n;
}
