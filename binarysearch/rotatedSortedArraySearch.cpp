int findPivotIndex(const vector<int> &A, int n) {
    int start = 0, end = n-1;

    while(start < end) {
        int mid = (start + end) / 2;
        if( (mid == 0 || (mid > 0 && A[mid-1] > A[mid]) ) && 
            (mid == n-1 || (mid < n-1 && A[mid+1] > A[mid]) ) ) {
                return mid;
            }
        if( A[mid] > A[0] ) {
            start = mid;
        } else {
            end = mid;
        }
    }
}

int findElement(const vector<int> &A, int start, int end, int element) {

    while(start <= end) {
        int mid = (start + end) / 2;

        if(A[mid] == element)    return mid;

        if(A[mid] > element){
            end = mid - 1;
        } else {
            start = mid + 1;
        }
    }
    return -1;
}

int Solution::search(const vector<int> &A, int B) {
    int n = A.size();
    if(A[0] < A[n-1])   return findElement(A, 0, n-1, B);
    
    int pivotInd = findPivotIndex(A, n);
    int ans = findElement(A, 0, pivotInd - 1, B);
    if(ans != -1)    return ans;
    return findElement(A, pivotInd, n-1, B);
}
