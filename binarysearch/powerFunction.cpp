int Solution::pow(int x, int n, int d) {

	if(x == 0)	return 0;

    if(n == 0) return 1;

    long long valueForHalfPower = pow(x, n/2, d);

    int ans = (n % 2) ?
    		( ( ( valueForHalfPower * valueForHalfPower ) % d ) * x ) % d :
    		( valueForHalfPower * valueForHalfPower ) % d ;

    return ans >= 0 ? ans : d + ans ;
}
