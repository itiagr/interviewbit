stack<int> st, minSt;

MinStack::MinStack() {
	while(!st.empty()) {
		st.pop();
	}

	while(!minSt.empty()) {
		minSt.pop();
	}
}

void MinStack::push(int x) {
	st.push(x);
	minSt.push(min(x, (minSt.empty() ? x : minSt.top())));
}

void MinStack::pop() {
	if(!st.empty()) {
		st.pop();
	}

	if(!minSt.empty()) {
		minSt.pop();
	}
}

int MinStack::top() {
	return st.empty() ? -1 : st.top();
}

int MinStack::getMin() {
	return minSt.empty() ? -1 : minSt.top();
}

