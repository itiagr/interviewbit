int Solution::braces(string A) {

	stack<char> stk;
	int len = A.length();

	for(int i = 0 ; i < len ; i++) {
		if(A[i] == ')') {

			if(stk.empty())	return 1;

			if(stk.top() == '('){
				return 1;
			}

			while(!stk.empty() && stk.top() != '(') {
				stk.pop();
			}
			stk.pop();
		} else if(A[i] == '*' || A[i] == '+' || A[i] == '-' || A[i] == '/' || A[i] == '(') {
			stk.push(A[i]);
		}
	}
	return 0;
}