int Solution::evalRPN(vector<string> &A) {
	stack<int> st;

	int n = A.size();

	for(int i = 0 ; i < n ; i++) {

		// if(!st.empty()) {
		// 	cout<<st.top()<<endl;
		// }

		if(A[i].compare("*") == 0 || A[i].compare("+") == 0 || A[i].compare("-") == 0 || A[i].compare("/") == 0) {
			int b = st.top();
			st.pop();
			int a = st.top();
			st.pop();
			if(A[i].compare("*") == 0) {
				st.push(a * b);
			} else if (A[i].compare("+") == 0) {
				st.push(a + b);
			} else if(A[i].compare("-") == 0) {
				st.push(a - b);
			} else if(A[i].compare("/") == 0) {
				st.push(a / b);
			}
		} else {
			st.push(stoi(A[i]));
		}
	}

	return st.top();
}
