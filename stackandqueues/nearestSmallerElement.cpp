vector<int> Solution::prevSmaller(vector<int> &A) {
	stack<int> st;
	int n = A.size();

	vector<int> ans;

	for(int i = 0 ; i < n ; i++) {
		while(!st.empty() && st.top() >= A[i]) {
			st.pop();
		}

		if(!st.empty()) {
			ans.push_back(st.top());
		} else {
			ans.push_back(-1);
		}

		st.push(A[i]);
	}

	return ans;
}
