int Solution::trap(const vector<int> &A) {
	int n = A.size(), ans = 0;
	deque<int> leftMax, rightMax;

	leftMax.push_back(A[0]);
	rightMax.push_front(A[n-1]);

	for(int i = 1 ; i < n ; i++) {
		leftMax.push_back(max(leftMax[i-1], A[i]));
		rightMax.push_front(max(rightMax[0], A[n-i-1]));
	}

	for(int i = 0 ; i < n ; i++) {
		ans += min(leftMax[i], rightMax[i]) - A[i];
	}

	return ans;
}
