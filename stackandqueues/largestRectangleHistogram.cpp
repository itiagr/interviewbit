int Solution::largestRectangleArea(vector<int> &A) {
	int n = A.size(), ans = INT_MIN;

	stack<int> st;

	int i = 0;
	while(i < n) {

		if(st.empty() || A[st.top()] <= A[i]) {
			st.push(i++);
		} else {
			int prev = st.top();
			st.pop();

			int width = st.empty() ? i : (i - st.top() - 1) ;
			ans = max(ans, width * A[prev]);
		}
	}

	while(!st.empty()) {
		int prev = st.top();
		st.pop();

		int width = st.empty() ? i : (i - st.top() - 1) ;
		ans = max(ans, width * A[prev]);
	}

	return ans;
}
