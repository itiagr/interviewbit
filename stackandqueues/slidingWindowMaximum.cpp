vector<int> Solution::slidingMaximum(const vector<int> &A, int B) {
	deque<int> dMax;
	deque<int> dMaxIndex;
	vector<int> ans;
	int n = A.size();

	for(int i = 0 ; i < n ; i++) {

		while(!dMax.empty() && dMax.back() <= A[i]) {
			dMax.pop_back();
			dMaxIndex.pop_back();
		}

		dMax.push_back(A[i]);
		dMaxIndex.push_back(i);

		if(!dMaxIndex.empty() && dMaxIndex.front() <= i - B) {
			dMaxIndex.pop_front();
			dMax.pop_front();
		}

		if(i >= B - 1) {
			ans.push_back(dMax.front());
		}
	}

	return ans;
}
