string Solution::simplifyPath(string A) {
	stack<string> st;
	int s = A.length();

	string curDir = "";
	for(int i = 0 ; i < s ; i++) {
		if(A[i] == '/') {
			if(!curDir.empty()) {
				if(curDir.compare("..") == 0 && !st.empty()) {
					st.pop();
				} else if(curDir.compare(".") != 0 && curDir.compare("..") != 0) {
					st.push(curDir);
				}
			}
			curDir = "";
		} else {
			curDir += (A[i]);
		}
	}

	if(!curDir.empty()) {
		if(curDir.compare("..") == 0 && !st.empty()) {
			st.pop();
		} else if(curDir.compare(".") != 0 && curDir.compare("..") != 0) {
			st.push(curDir);
		}
	}

	if(st.empty())	return "/";
	string ans = "";
	while(!st.empty()) {
		ans = ("/" + st.top()) + ans;
		st.pop();
	}

	return ans;
}
