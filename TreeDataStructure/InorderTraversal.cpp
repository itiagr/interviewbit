/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

//with recursion
void inorderTraversalHelper(vector<int> &traversal, TreeNode* curNode) {
	if(curNode == NULL)	return;
	inorderTraversalHelper(traversal, curNode->left);
	traversal.push_back(curNode->val);
	inorderTraversalHelper(traversal, curNode->right);
}

vector<int> Solution::inorderTraversal(TreeNode* A) {
	vector<int> traversal;
	
	stack<TreeNode*> stk;

	TreeNode* cur = A;
	while(cur != NULL || !stk.empty()) {

		if(cur == NULL) {
			cur = stk.top();
			stk.pop();

			traversal.push_back(cur->val);

			cur = cur->right;
		} else {
			stk.push(cur);
			cur = cur->left;
		}
	}

	return traversal;
}
