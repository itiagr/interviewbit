/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

int height(TreeNode* node) {
	if(node == NULL)	return 0;

	return 1 + max(height(node->left) , height(node->right));
}

int Solution::maxDepth(TreeNode* A) {
	return height(A);
}