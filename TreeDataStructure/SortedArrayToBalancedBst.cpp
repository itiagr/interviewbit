/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

TreeNode* sortedArrayToBSTHelper(const vector<int> &A, int start, int end) {
	if(start > end)	return NULL;

	int mid = (start + end) / 2;

	TreeNode* nodeToInsert = new TreeNode(A[mid]);
	nodeToInsert->left = sortedArrayToBSTHelper(A, start, mid - 1);
	nodeToInsert->right = sortedArrayToBSTHelper(A, mid + 1, end);

	return nodeToInsert;
}

TreeNode* Solution::sortedArrayToBST(const vector<int> &A) {
    int n = A.size();

    if(n == 0)	return NULL;

    return sortedArrayToBSTHelper(A, 0, n - 1);
}
