struct TrieNode{
	map<char,TrieNode*> children;
	bool isWord;

	TrieNode() : isWord(false) {};
};

vector<string> generateWords(string str) {
	str += "_";
	int n = str.length();

	vector<string> tokens;
	string word = "";

	for(int i = 0 ; i < n ; i++) {
		if(str[i] == '_' ) {
			if(word.length() > 0) {
				tokens.push_back(word);
				word = "";
			}
		} else {
			word += str[i];
		}
	}

	return tokens;
} 

void constructTrie(string str, TrieNode* head) {
	str += "_";
	int n = str.length();

	TrieNode* cur = head;

	for(int i = 0 ; i < n ; i++) {
		if(str[i] == '_') {
			cur->isWord = true;
			cur = head;
		} else {
			if((cur->children).find(str[i]) != (cur->children).end()) {
				cur = (cur->children)[str[i]];
			} else {
				TrieNode* newNode = new TrieNode();
				(cur->children)[str[i]] = newNode;
				cur = newNode; 
			}
		}
	}
}

bool findGoodWord(TrieNode* head, string goodWord) {
	int n = goodWord.length();

	for(int i = 0 ; i < n ; i++) {
		if((head->children).find(goodWord[i]) == (head->children).end()) {
			return false;
		} else {
			head = (head->children)[goodWord[i]];
		}
	}

	return head->isWord;
}

bool sortHelper(pair<int, int> firstPair, pair<int, int> secondPair) {
	return (firstPair.first > secondPair.first) || 
		(firstPair.first == secondPair.first && firstPair.second < secondPair.second);
}

vector<int> Solution::solve(string A, vector<string> &B) {

	TrieNode* trie = new TrieNode();
	constructTrie(A, trie);

	int numReviews = B.size();

	vector< pair<int, int> > goodWordsForReviews;

	for(int i = 0 ; i < numReviews ; i++) {
		vector<string> reviewWords = generateWords(B[i]);
		int nWords = reviewWords.size();
		int goodWords = 0;
		for(int j = 0 ; j < nWords ; j++) {
			if(findGoodWord(trie, reviewWords[j])) {
				goodWords++;
			}
		}
		goodWordsForReviews.push_back(make_pair(goodWords, i));
	}

	sort(goodWordsForReviews.begin(), goodWordsForReviews.end(), sortHelper);
	vector<int> result;

	for(int i = 0 ; i < numReviews ; i++) {
		result.push_back(goodWordsForReviews[i].second);
	}

	return result;

}
