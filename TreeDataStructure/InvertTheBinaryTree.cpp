/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

void swapNodes(TreeNode* node) {
	if(node == NULL)	return;
	TreeNode* tmp = node->left;
	node->left = node->right;
	node->right = tmp;
	swapNodes(node->left);
	swapNodes(node->right);
}

TreeNode* Solution::invertTree(TreeNode* A) {
	swapNodes(A);
	return A;
}
