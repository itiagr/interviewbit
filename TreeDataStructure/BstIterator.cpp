/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

stack<TreeNode*> nextNodeStack;

BSTIterator::BSTIterator(TreeNode *root) {
	nextNodeStack = stack<TreeNode*>();

	while(root != NULL) {
		nextNodeStack.push(root);
		root = root->left;
	}

}

/** @return whether we have a next smallest number */
bool BSTIterator::hasNext() {
	return !nextNodeStack.empty();
}

/** @return the next smallest number */
int BSTIterator::next() {
	TreeNode* nextNode = nextNodeStack.top();
	nextNodeStack.pop();

	TreeNode* node = nextNode->right;
	while(node != NULL) {
		nextNodeStack.push(node);
		node = node->left;
	}

	return nextNode->val;
}

/**
 * Your BSTIterator will be called like this:
 * BSTIterator i = BSTIterator(root);
 * while (i.hasNext()) cout << i.next();
 */
