/**
 * Definition for binary tree with next pointer.
 * struct TreeLinkNode {
 *  int val;
 *  TreeLinkNode *left, *right, *next;
 *  TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
 * };
 */
void Solution::connect(TreeLinkNode* A) {
	queue<TreeLinkNode*> curLevel, nextLevel;
	curLevel.push(A);

	while(!curLevel.empty()) {

		TreeLinkNode* nodeToProcess = curLevel.front();
		curLevel.pop();

		if(nodeToProcess->left != NULL) {
			nextLevel.push(nodeToProcess->left);
		}

		if(nodeToProcess->right != NULL) {
			nextLevel.push(nodeToProcess->right);
		}

		if(!curLevel.empty()) {
			nodeToProcess->next = curLevel.front();
		} else {
			nodeToProcess->next = NULL;
			while(!nextLevel.empty()) {
				curLevel.push(nextLevel.front());
				nextLevel.pop();
			}
		}
	}
}
