/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

void traversalHelper(vector<int> &traversal, TreeNode* curNode) {
	if(curNode == NULL)	return;
	traversal.push_back(curNode->val);
	traversalHelper(traversal, curNode->left);
	traversalHelper(traversal, curNode->right);
}

vector<int> Solution::preorderTraversal(TreeNode* A) {
	vector<int> traversal;
	traversalHelper(traversal, A);
	return traversal;
}
