/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

map<TreeNode* , int > heightDP;

int height(TreeNode* node) {
	if(node == NULL)	return 0;
	if(heightDP.find(node) != heightDP.end()) {
		return heightDP[node];
	}

	int heightOfNode = 1 + max(height(node->left) , height(node->right));
	heightDP[node] = heightOfNode;
	return heightOfNode;
}

int findIfBalanced(TreeNode* head) {
	if(head == NULL)	return 1;
	int leftSubTreeHeight = height(head->left);
	int rightSubTreeHeight = height(head->right);

	return (abs(leftSubTreeHeight - rightSubTreeHeight) <= 1) && findIfBalanced(head->left) && findIfBalanced(head->right);
}

int Solution::isBalanced(TreeNode* A) {
	heightDP.clear();
	return findIfBalanced(A);
}
