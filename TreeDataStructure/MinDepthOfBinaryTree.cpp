/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

int height(TreeNode* node) {
	if(node == NULL)	return 0;

	if(node->left == NULL && node->right == NULL)	return 1;

	if(node->left == NULL)	
		return 1 + height(node->right);

	if(node->right == NULL)
		return 1 + height(node->left);

	return 1 + min(height(node->left) , height(node->right));
}

int Solution::minDepth(TreeNode* A) {
	return height(A);
}