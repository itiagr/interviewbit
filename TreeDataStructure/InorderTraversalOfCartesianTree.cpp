/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

int findMaxNumber(vector<int> &A, int start, int end) {
	int maxNumber = A[start];
	int maxNumberIndex = start;

	for(int i = start + 1 ; i <= end ; i++) {
		if(A[i] > maxNumber) {
			maxNumber = A[i];
			maxNumberIndex = i;
		}
	}

	return maxNumberIndex;
}

TreeNode* buildBinaryTree(vector<int> &A, int start, int end) {
	if(start > end)	return NULL;

	int maxNumberIndex = findMaxNumber(A, start, end);

	TreeNode* newNode = new TreeNode(A[maxNumberIndex]);
	newNode->left = buildBinaryTree(A, start, maxNumberIndex - 1);
	newNode->right = buildBinaryTree(A, maxNumberIndex + 1, end);

	return newNode;
}

TreeNode* Solution::buildTree(vector<int> &A) {
	int n = A.size();

	return buildBinaryTree(A, 0, n - 1);
}
