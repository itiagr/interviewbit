/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

void inorderTraversalHelper(vector<int> &traversal, TreeNode* curNode, int k) {
	if(curNode == NULL || traversal.size() == k)	return;
	inorderTraversalHelper(traversal, curNode->left, k);
	traversal.push_back(curNode->val);
	inorderTraversalHelper(traversal, curNode->right, k);
}

int Solution::kthsmallest(TreeNode* A, int k) {
	vector<int> traversal;
	inorderTraversalHelper(traversal, A, k);
	return traversal[k - 1];
}
