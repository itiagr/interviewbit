/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

vector<vector<int> > Solution::verticalOrderTraversal(TreeNode* A) {
	map<int, vector<int> > traversals;

	queue<pair<TreeNode* , int> > nodesToProcess;
	nodesToProcess.push(make_pair(A, 0));

	while(!nodesToProcess.empty()) {
		TreeNode* curNode = nodesToProcess.front().first;
		int curOrder = nodesToProcess.front().second;

		nodesToProcess.pop();

		if(curNode == NULL)	continue;

		if(traversals.find(curOrder) != traversals.end()) {
			traversals[curOrder].push_back(curNode->val);
		} else {
			traversals[curOrder] = vector<int>{curNode->val};
		}
		
		nodesToProcess.push(make_pair(curNode->left, curOrder - 1));
		nodesToProcess.push(make_pair(curNode->right, curOrder + 1));
	}

	vector<vector<int> > ans;
	map<int, vector<int> >::iterator it;

    for(it = traversals.begin() ; it != traversals.end() ; it++) {
        ans.push_back(it->second);
    }

    return ans;
}
