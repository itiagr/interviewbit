/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

void traversalHelper(vector<int> &traversal, TreeNode* curNode) {
	if(curNode == NULL)	return;
	traversalHelper(traversal, curNode->left);
	traversalHelper(traversal, curNode->right);
	traversal.push_back(curNode->val);
}

vector<int> Solution::postorderTraversal(TreeNode* A) {
	vector<int> traversal;
	traversalHelper(traversal, A);
	return traversal;
}
