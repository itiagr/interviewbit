/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

TreeNode* getRightMostNode(TreeNode* headNode) {
	if(headNode->right == NULL)	return headNode;
	return getRightMostNode(headNode->right);
}

TreeNode* Solution::flatten(TreeNode* headNode) {
    if(headNode == NULL )	return headNode;

    if(headNode->left != NULL) {
    	flatten(headNode->left);
    }

    if(headNode->right != NULL) {
    	flatten(headNode->right);
    }

    if(headNode->left != NULL && headNode->left->left == NULL) {
    	TreeNode* tmp = headNode->right;
    	TreeNode* righMostNodeOfLeftTree = getRightMostNode(headNode->left);

    	headNode->right = headNode->left;
    	righMostNodeOfLeftTree->right = tmp;
    	headNode->left = NULL;
    }

    //cout<<headNode->right<<endl;
    return headNode;
}
