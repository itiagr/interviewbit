/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

int hasPathSumHelper(TreeNode* headNode, int sum) {
	if(headNode->left == NULL && headNode->right == NULL) {
		return headNode->val == sum;
	}

	if(headNode->left == NULL) {
		return hasPathSumHelper(headNode->right, sum - headNode->val);
	}

	if(headNode->right == NULL) {
		return hasPathSumHelper(headNode->left, sum - headNode->val);
	}

	return hasPathSumHelper(headNode->left, sum - headNode->val)
			|| hasPathSumHelper(headNode->right, sum - headNode->val);
}

int Solution::hasPathSum(TreeNode* headNode, int sum) {
	if(headNode == NULL) {
		return sum == 0; 
	}

	return hasPathSumHelper(headNode, sum);
}
