/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

void swapNodes(TreeNode* node) {
	if(node == NULL)	return;
	TreeNode* tmp = node->left;
	node->left = node->right;
	node->right = tmp;
	swapNodes(node->left);
	swapNodes(node->right);
}

int isSameTree(TreeNode* firstNode, TreeNode* secondNode) {
	if(firstNode == NULL && secondNode == NULL)	return 1;

	if(firstNode == NULL || secondNode == NULL)	return 0;

	return (firstNode->val == secondNode->val 
		&& isSameTree(firstNode->left, secondNode->left) 
		&& isSameTree(firstNode->right, secondNode->right));
}

int Solution::isSymmetric(TreeNode* node) {
	if(node == NULL)	return 1;

	swapNodes(node->right);

	return isSameTree(node->left, node->right);
}
