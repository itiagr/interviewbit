/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
int Solution::isSameTree(TreeNode* firstNode, TreeNode* secondNode) {
	if(firstNode == NULL && secondNode == NULL)	return 1;

	if(firstNode == NULL || secondNode == NULL)	return 0;

	return (firstNode->val == secondNode->val 
		&& isSameTree(firstNode->left, secondNode->left) 
		&& isSameTree(firstNode->right, secondNode->right));
}
