/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

int find(TreeNode* headNode, TreeNode* firstNode, int elem) {
	if(headNode == NULL)	return 0;

	return (headNode != firstNode && headNode->val == elem) 
		|| ( (elem > headNode->val) ? find(headNode->right, firstNode, elem) : find(headNode->left, firstNode, elem) );
}

int ifExistsTwoNumbers(TreeNode* headNode, TreeNode* curNode, int sum) {
	if(curNode == NULL)	return 0;
	
	return find(headNode, curNode, sum - curNode->val) 
		|| ifExistsTwoNumbers(headNode, curNode->left, sum) 
		|| ifExistsTwoNumbers(headNode, curNode->right, sum);
}

int Solution::t2Sum(TreeNode* headNode, int sum) {
	return ifExistsTwoNumbers(headNode, headNode, sum);
}
