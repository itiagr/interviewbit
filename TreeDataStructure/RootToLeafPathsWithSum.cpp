/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */



vector<vector<int> > Solution::pathSum(TreeNode* headNode, int sum) {
	if(headNode == NULL) return vector<vector<int> >();
	vector<int> rootToLeafPath;
	vector<vector<int> > rootToLeafPaths;

	hasPathSumHelper(headNode, rootToLeafPath, rootToLeafPaths, sum);

	return rootToLeafPaths;
}