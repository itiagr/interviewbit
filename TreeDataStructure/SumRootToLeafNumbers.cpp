/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

const int MOD = 1003;

long long hasPathSumHelper(TreeNode* headNode, long long number) {

	if(headNode == NULL)	return 0;

	number = (number * 10 + headNode->val ) % MOD;

	if(headNode->left == NULL && headNode->right == NULL) {
		return number;
	}

	long long answer = 0;
	if(headNode->left != NULL) {
		answer = hasPathSumHelper(headNode->left, number) % MOD;
	}

	if(headNode->right != NULL) {
		answer = (answer + hasPathSumHelper(headNode->right, number) ) % MOD;
	}
	return answer;
}

int Solution::sumNumbers(TreeNode* headNode) {
	return hasPathSumHelper(headNode, 0);
}
