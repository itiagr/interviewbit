vector<int> Solution::dNums(vector<int> &A, int k) {
	int n = A.size();

	if(k > n)	return vector<int>();

	vector<int> ans;

	map<int, int> frequency;
	int distinctNums = 0;

	for(int i = 0 ; i < k ; i++) {

		if(frequency.find(A[i]) == frequency.end() || frequency[A[i]] == 0) {
			distinctNums++;
			frequency[A[i]] = 1;
		} else {
			frequency[A[i]]++;
		}
	}

	ans.push_back(distinctNums);

	for(int i = k ; i < n ; i++) {
		frequency[A[i - k]]--;

		if(frequency[A[i - k]] == 0) {
			distinctNums--;
		}

		if(frequency.find(A[i]) == frequency.end() || frequency[A[i]] == 0) {
			distinctNums++;
			frequency[A[i]] = 1;
		} else {
			frequency[A[i]]++;
		}

		ans.push_back(distinctNums);
	}

	return ans;
}
