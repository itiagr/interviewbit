/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

ListNode* mergeTwoLists(ListNode* head1, ListNode* head2) {

	if(head1 == NULL)	return head2;
	if(head2 == NULL)	return head1;

	ListNode* head = NULL;
	ListNode* cur = NULL;
	while(head1 != NULL && head2 != NULL) {
		if(head1->val < head2->val) {
			if(head == NULL) {
				head = head1;
				cur = head1;
			} else {
				cur->next = head1;
				cur = cur->next;
			}
			head1 = head1->next;
		} else {
			if(head == NULL) {
				head = head2;
				cur = head2;
			} else {
				cur->next = head2;
				cur = cur->next;
			}
			head2 = head2->next;
		}
	}

	if(head1 != NULL) {
		cur->next = head1;
	}

	if(head2 != NULL) {
		cur->next = head2;
	}

	return head;
}

ListNode* Solution::mergeKLists(vector<ListNode*> &A) {
    ListNode* finalHead = NULL;

    int n = A.size();

    if(n == 0)	return finalHead;

    for(int i = 0 ; i < n ; i++) {
    	finalHead = mergeTwoLists(finalHead, A[i]);
    	//cout<<"yes"<<endl;
    }

    return finalHead;
}
