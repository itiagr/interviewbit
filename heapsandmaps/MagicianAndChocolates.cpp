int Solution::nchoc(int k, vector<int> &bags) {
	const int MOD = 1000000007;
	priority_queue<int> pq;
	int n = bags.size();

	for(int i = 0 ; i < n ; i++) {
		pq.push(bags[i]);
	}

	long long ans = 0;

	while(k) {
		int top = pq.top();
		pq.pop();

		ans += top;
		ans = ans % MOD;
		pq.push(top / 2);

		k--;
	}

	return ans;
}
