int maxCapacity = 0;

map<int, int> cache;
vector<int> recents;

LRUCache::LRUCache(int capacity) {
	maxCapacity = capacity;
	cache.clear();
	recents.clear();
}

void pushToRecents(int key) {
	vector<int>::iterator position = std::find(recents.begin(), recents.end(), key);
	if(position != recents.end()) {
		recents.erase(position);
	}
	recents.push_back(key);
}

int LRUCache::get(int key) {
	if(cache.find(key) != cache.end()) {
		pushToRecents(key);
		return cache[key];
	} else {
		return -1;
	}
}

void LRUCache::set(int key, int value) {

	if(cache.find(key) != cache.end()) {
		cache[key] = value;
		pushToRecents(key);
		return;
	}

	if(cache.size() == maxCapacity) {
		cache.erase(recents[0]);
		recents.erase(recents.begin());
	}
	cache[key] = value;
	pushToRecents(key);
}
