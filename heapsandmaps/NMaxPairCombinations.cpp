vector<int> Solution::solve(vector<int> &A, vector<int> &B) {
	int n = A.size();

	if(n == 0)	return vector<int>();

	sort(A.begin(), A.end());
	sort(B.begin(), B.end());

	vector<int> ans;

	priority_queue< pair< int, pair< int, int> > > pq;
	set< pair< int, int> > setForPair;

	pq.push(make_pair(A[n - 1] + B[n - 1] , make_pair(n - 1 , n - 1)));
	setForPair.insert(make_pair(n - 1 , n - 1));

	int k = n;

	while(k) {
		pair< int, pair< int, int> > topValue = pq.top();
		pq.pop();
		ans.push_back(topValue.first);

		int ind1 = topValue.second.first;
		int ind2 = topValue.second.second;

		pair< int, int> p1 = make_pair(ind1 - 1, ind2);
		pair< int, int> p2 = make_pair(ind1 , ind2 - 1);

		if(ind1 > 0 && setForPair.find(p1) == setForPair.end()) {
			pq.push(make_pair(A[ind1 - 1] + B[ind2] , p1));
			setForPair.insert(p1);
		}

		if(ind2 > 0 && setForPair.find(p2) == setForPair.end()) {
			pq.push(make_pair(A[ind1] + B[ind2 - 1], p2));
			setForPair.insert(p2);
		}

		k--;
	}

	return ans;
}
